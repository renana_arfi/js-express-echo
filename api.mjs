import express from "express";
import log from "@ajar/marker";

const router = express.Router();

const payment = (req,res,next)=>{
    log.magenta("payment process start now");
    next();
}

const myLog = (req,res,next)=>{
    log.yellow("i'm in mylog");
    res.redirect("/login");
    // next();
}

router.get('/', async (req,res)=>{
    res.json({msg:"main api route"})
})

router.get("/pop", (req,res)=>{
    res.json({msg:"pop api route"})

})

router.get('/payed', payment,myLog,(req,res)=>{
    res.json({msg:"payed finish"})
})

export default router;