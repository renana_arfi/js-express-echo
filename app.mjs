import express from "express";
import log from "@ajar/marker";
import morgan from "morgan";

import apiRouter from "./api.mjs";
import webRouter from "./web.mjs";

const { PORT = 3030, HOST = "localhost" } = process.env;

const app = express();

app.use('/api',apiRouter);
app.use('/',webRouter);

app.use(express.json());


const logger = (req,res,next)=> {
    log.info(`my host:${ req.host } and my port:  ${ req.p }`)
    next()
}

app.use( logger )

// return html markup response
// '/hola?food=burger&town=ashdod'
app.get("/hola", (req, res) => {
  const markup = `<h1>Hello Express</h1>
                    <p>This is an example demonstrating some basic html markup<br/>
                        being sent and rendered in the browser</p>`;
  res.status(200).set("Content-Type", "text/html").send(markup);
});

app.get("/", (req, res, next) => {
  res.status(200).send("Hello Express!");
  next();
});

app.get("/users", (req, res) => {
  res.status(200).send("Get all Users");
});

//req.query - access the querystring part of the request url
// http://localhost:3030/search?food=burger&town=ashdod'
app.get("/search", (req, res) => {
  log.magenta("query string place: ", req.query.food);
  log.magenta("query string food: ", req.query.town);
  res.status(200).json(req.query);
});

//req.params - access dynamic parts of the url
//http://localhost:3030/resturants/burger/ashdod
app.get("/resturants/:food/:town", (req, res) => {
  res.status(200).send(`<h1>parm food #${req.params.food}</h1> 
                    parm town: ${req.params.town}`);
});

//req.body - access the request body of a POST request
//http://localhost:3030/shows/food=burger&town=ashdod
app.post("/shows", (req, res) => {
  //instead of saving the sent data to a DB, we'll just display it
  // log.info(req.body);
  log.obj(req.body, "req.body is:");
  res
    .status(200)
    .send(`Creating a new Show by the name of: ${req.body.showName}.`);
});

app.use(morgan("dev"));

app.use((req, res) => {
  res.status(404).send(` - 404 - url ${req.url} was not found`);
});

app.listen(PORT, HOST, () => {
  log.magenta(`🌎  listening on`, `http://${HOST}:${PORT}`);
});

